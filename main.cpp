#include <bits/stdc++.h>

using namespace std;

struct point {
    int count;
    string prev;
    string next;
};

void makePoint(point &p) {
    p.count = 0;
}

// tipe bentukan untuk menyimpan 2char dan jumlahnya
struct datum {
    int count; // jumlah hitungan
    string str; // nilai 2 char
    vector<point> position; // digunakan untuk menyimpan posisi char 2
};

// mengecek apakah 2 char sudah ada di data
// jika ada maka mengembalikan index tempat ditemukan di data
int isExist(vector<datum> d, string str) {

    int idx = -1;

    for (int i = 0; i < d.size(); i++) {
        if (str.compare(d[i].str) == 0){
            idx = i; 
            break;
        } 
    }

    return idx;
}

//mengecek suatu karakter adalah termasuk dalam alfabet
bool isValid (char c) {
    return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '\'' || c == '-');
}

//membaca data dari file eksternal
void readRawData(string fileName, vector<string> &raw_data) {
    fileName.c_str();

    // Pembacaan file eksternal
    std::ifstream infile(fileName.c_str());
    std::string line;
    //baca per line
    while (std::getline(infile, line)){
        raw_data.push_back(line);
    }
}


// mencetak data ke terminal
void printData(vector<datum> data) {
    for (int i = 0; i < data.size(); i++) {
        cout << "word: " << data[i].str << "\t jumlah: " << data[i].count << endl;
        for (int j = 0; j < data[i].position.size(); j++) {
            string prev = data[i].position[j].prev;
            string next = data[i].position[j].next;
            if (prev.compare("") == 0) 
                cout << "\t prev: " << "blank" << endl;
            else
                cout << "\t prev: " << prev << endl;
            
            if (next.compare("") == 0)
                cout << "\t next: " << "blank" << endl;
            else
                cout << "\t next: " << next << endl;

            cout << "\t \t count: " << data[i].position[j].count << endl;
        }
    }
}

// mengurutkan data sesuai jumlah count
void sortData(vector<datum> &data) {
    for (int i = 0; i < data.size()-1; i++) {
        for (int j = i + 1; j < data.size(); j++) {
            if (data[i].count < data[j].count) {
                datum d = data[i];
                data[i] = data[j];
                data[j] = d;
            }
        }
    }
    for (int i = 0; i < data.size(); i++) {
        for (int m = 0; m < data[i].position.size() - 1; m++) {
            for (int n = m + 1; n < data[i].position.size(); n++) {
                if (data[i].position[m].count < data[i].position[n].count) {
                    point p = data[i].position[m];
                    data[i].position[m] = data[i].position[n];
                    data[i].position[n] = p;
                }
            }
        }
    }
}

// menyimpan data ke file eksternal
void saveData(vector<datum> data, string fileName) {
    ofstream file;
    file.open(fileName.c_str());
    // for (int i = 0; i < 10; i++) {
    for (int i = 0; i < data.size(); i++) {
        file << "char: " << data[i].str << "\t jumlah: " << data[i].count << endl;
        for (int j = 0; j < data[i].position.size(); j++) {
            string p = data[i].position[j].prev;
            string n = data[i].position[j].next;
            // file << "\t prev: " << p << endl;
            // file << "\t next: " << n << endl;
            file << "\t prev: ";
            if (p.compare("") == 0) {
                file << "blank" << endl;
            } else {
                file << p << endl;
            }

            file << "\t next: ";
            if (n.compare("") == 0) {
                file << "blank" << endl;
            }
            else {
                file << n << endl;
            }
            file << "\t \t count: " << data[i].position[j].count << endl;
        }
    }
    file.close();
}

string getPreviosString(string str, int idx) {
    string prev = "";
    string prevReverse = "";
    int i;
    if (idx == 0)
        return "";
    else
        i = idx-1;

    // terbalik
    while (isValid(str[i])) {
        prev.append(1, str[i]);
    }
        i--;

    for (int i = prev.size()-1 ; i >= 0 ; i--) {
        prevReverse.append(1, prev[i]);
    }

    return prevReverse; 
}

string getWord(string str, int &idx) {
    
    string word = "";

    if (idx >= str.size() - 1)
        return word;
    
    while (isValid(str[idx])) {
        word.append(1, str[idx]);
        idx++;
    }

    while (!isValid(str[idx])) {
        idx++;
    }
}

string getNextString(string str, int idx) {
    string next = "";
    
    if (idx >= str.size()-1)
        return "";
        
    while (isValid(str[idx])) {
        next.append(1, str[idx]);
        idx++;
    }

    return next;
    
}

int main() {

    // Data untuk menyimpan 2str dan jumlahnya
    vector<datum> data;

    vector<string> raw_data;

    readRawData("corpus.txt", raw_data);

    string line;

    for (int i = 0; i < raw_data.size(); i++) {     // comment baris ini jika tidak ingin mencoba dari semua kalimat
    // for (int i = 0; i < 1; i++) {            // uncomment baris ini jika hanya ingin mencoba satu kalimat
        line = raw_data[i];
        // cout << i << endl;
        // cout << line << endl;
        
        int k = 0;
        string prev = "";

        while (k < line.size()) {
            string str = getWord(line, k);
            // cout << str << endl;
            // cout << k << endl;
            // cout << "Prev: " << prev << endl;

            int val_idx = isExist(data, str);

            if (val_idx == -1) {
                datum instance;
                instance.count = 1;
                instance.str = str;
                point p;
                makePoint(p);
                p.count = 1;
                p.prev = prev;
                p.next = getNextString(line, k);
                instance.position.push_back(p);
                data.push_back(instance);

            } else {

                data[val_idx].count++;
                point p;
                makePoint(p);
                p.count = 1;
                p.prev = prev;
                p.next = getNextString(line, k);
                bool cek  = false;
                int idx_pos;
                for (int k =0; k < data[val_idx].position.size() && !cek; k++) {
                    if (p.prev.compare(data[val_idx].position[k].prev) == 0 && p.next.compare(data[val_idx].position[k].next) == 0) {
                        cek = true;
                        idx_pos = k;
                    }
                }
                if (cek) {
                    data[val_idx].position[idx_pos].count++;
                } else {
                    data[val_idx].position.push_back(p);
                }
            }
            prev = str;
        }
    }

    sortData(data);

    // printData(data);

    saveData(data, "output.txt");

    return 0;
}