## Milestone Generate Kalimat - 2 Pasang Char

### Deskripsi
Menentukan jumlah 2 char berpasangan dari kumpulan kalimat. Asumsi hanya digunakan alfabet saja yaitu a s.d. z dan A s.d. Z. Karakter numerik, spasi, koma, titik, dll akan diabaikan.

### Penjelasan
Program akan membaca kumpulan kalimat dari file `corpus.txt`. Output program dituliskan pada file `output.txt`

### Cara Penggunaan

```
g++ main.cpp -o main
```
